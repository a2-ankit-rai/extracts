Optimizing for learning is a high-leverage activity for the effective engineer.

How we view our own intelligence, character, and abilities profoundly affects how we lead our lives; it largely determines whether we remain stuck in our current situations or achieve what we value.

**People adopt one of two mindsets, which in turn affects how they view effort and failure.**

1. People with a **fixed mindset** believe that “human qualities are carved in stone” and that they’re born with a predetermined amount of intelligence—either they’re smart or they’re not. Failure indicates they’re not, so they stick with the things they do well—things that validate their intelligence. They tend to give up early and easily, which enables them to point to a lack of effort rather than a lack of ability as causing failure.

2. On the other hand, those with a **growth mindset** believe that they can cultivate and grow their intelligence and skills through effort. They may initially lack aptitude in certain areas, but they view challenges and failures as opportunities to learn. As a result, they’re much less likely to give up on their paths to success.

Those with growth mindsets are willing to take steps to better themselves, whereas those with fixed mindsets are not.

The mindset we adopt about our effectiveness as engineers drastically shapes whether we learn and grow or let our skills plateau and stagnate. Do we treat our abilities as fixed quantities outside of our control? Or do we direct our efforts and our energy toward improving ourselves?

A growth mindset looks like. It means accepting responsibility for each aspect of a situation that you can change—anything from improving your conversational skills to mastering a new engineering focus—rather than blaming failures and shortcomings on things outside your control. It means taking control of your own story. It means optimizing for experiences where you learn rather than for experiences where you effortlessly succeed. And it means investing in your rate of learning.

Learning, like interest, also compounds. Therefore, the same three takeaways apply:

1. Learning follows an exponential growth curve. Knowledge gives you a foundation, enabling you to gain more knowledge even faster. For example, an understanding of recursion provides the basis for many other concepts, like trees and graph searches, which in turn are necessary for understanding compilers and network topologies.

2. The earlier that you optimize for learning, the more time your learning has to compound. A good first job, for example, makes it easier to get a better second job, which then affects future career opportunities.

3. Due to compounding, even small deltas in your own learning rate make a big difference over the long run.

# Dedicate Time on the Job to Develop New Skills

To invest in your own growth, you should carve out your own 20% time. It’s more effective to take it in one- or two-hour chunks each day rather than in one full day each week, because you can then make a daily habit out of improving your skills.

So what should you do with that 20% time? You can develop a deeper understanding of areas you’re already working on and tools that you already use.

**Adjacent Disciplines**

These are the disciplines related to your core role and where increased familiarity can make you more self-sufficient and effective.

- If you’re a product engineer, adjacent disciplines might include product management, user research, or even backend engineering.
- If you’re an infrastructure engineer, they might include machine learning, database internals, or web development.
- If you’re a growth engineer, adjacent disciplines might be data science, marketing, or behavioral psychology.

Knowledge in adjacent disciplines will not only be useful, but you’ll also be more likely to retain the information because you’ll be actively practicing it.

Ten suggestions to take advantage of the resources available to you at work:

1. Study code for core abstractions written by the best engineers at your company.

2. Write more code. Since actively programming expends more effort than passively reading code, you’ll find that practicing the craft is a high-leverage activity for improving your programming skills. Moreover, it’s easy to think you understand something you’ve read, only to find large knowledge gaps when you actually set out to do it.

3. Go through any technical, educational material available internally.

4. Master the programming languages that you use.

5. Send your code reviews to the harshest critics. Optimize for getting good, thoughtful feedback rather than for lowering the barrier to getting your work checked in. Ask for a more detailed review on those implementations you’re not as confident about. Discuss software designs with your company’s best designers in order to avoid writing great code for a design that doesn’t work well.

6. Participate in design discussions of projects you’re interested in.

7. Work on a diversity of projects. If you find yourself always doing similar tasks using similar methods, it’s going to be hard to pick up new skills. Interleaving different projects can teach you what problems are common across projects and what might just be artifacts of your current one. Moreover, research on learning confirms that the interleaved practice of different skills is more effective than repeated, massed practice of a single skill at preparing people to tackle unfamiliar problems.

8. Make sure you’re on a team with at least a few senior engineers whom you can learn from. If you’re not, consider changing projects or teams. This will help increase your learning rate for the remaining 80% of your time.

9. Jump fearlessly into code you don’t know. After years of observation, Bobby Johnson, a former engineering director at Facebook, concluded that engineering success was highly correlated with “having no fear in jumping into code they didn’t know.” Fear of failure often holds us back, causing us to give up before we even try. But as Johnson explains, “in the practice of digging into things you don’t know, you get better at coding.

**Research in positive psychology shows that continual learning is inextricably linked with increased happiness.**

1. Learn new programming languages and frameworks. Keep a running list of the programming languages, software tools, and frameworks that you want to learn, and set goals to spend time and master them.

2. Invest in skills that are in high demand. Read books.

3. Build and maintain a strong network of relationships.

4. Follow bloggers who teach. Write to teach.

5. Tinker on side projects.

# Key Takeaways

- Own your story. Focus on changes that are within your sphere of influence rather than wasting energy on blaming the parts that you can’t control. View failures and challenges through a growth mindset, and see them as opportunities to learn.
- Don’t shortchange your learning rate. Learning compounds like interest. The more you learn, the easier it is to apply prior insights and lessons to learn new things. Optimize for learning, particularly early in your career, and you’ll be best prepared for the opportunities that come your way.
- Find work environments that can sustain your growth. Interview the people at the team or company you’re considering. Find out what opportunities they provide for onboarding and mentoring, how transparent they are internally, how fast they move, what your prospective co-workers are like, and how much autonomy you’ll have.
- Capitalize on opportunities at work to improve your technical skills. Learn from your best co-workers. Study their code and their code reviews. Dive into any available educational material provided by your company, and look into classes or books that your workplace might be willing to subsidize.
- Locate learning opportunities outside of the workplace. Challenge yourself to become better by just 1% a day. Not all of your learning will necessarily relate to engineering skills, but being a happier and better learner will help you become a more effective engineer in the long run.
