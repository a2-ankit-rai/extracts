Prioritizing isn’t only relevant to user growth, however. In any engineering discipline (and in life), there will always be more tasks to do than you have time for. Working on one task means not working on another. Therefore, regular prioritization is a high-leverage activity, because it determines the leverage of the rest of your time. After all, if you work for weeks on a project that has little impact and garners few lessons, how is that different for your business than not working at all?

when it comes to your personal and professional goals, taking the time and energy to prioritize will significantly increase your chances for success.

# Track To-Dos in a Single, Easily Accessible List

No matter how much of an expert you might be, a well-designed checklist can significantly improve your outcomes.

Engineers can benefit from adopting checklists as well. The first step in effective prioritization is listing every task that you might need to do. As David Allen explains in Getting Things Done, this is because the human brain is optimized for processing and not for storage.

To-do lists should have two major properties: they should be a canonical representation of our work, and they should be easily accessible. A single master list is better than an assortment of sticky notes, sheets of paper, and emails, because these scattered alternatives are easily misplaced and make it harder for your brain to trust that they’re comprehensive. Having the master list easily accessible allows you to quickly identify a task you can complete if you unexpectedly have a block of free time. Plus, if you think up a new task, you can add it directly to your list even when you’re out and about, rather than investing mental energy in trying to remember it.

When you consistently offload to-dos to your list, you reduce the number of tasks you need to remember down to just one—to check your single, master list—and you free your mind to focus on a higher-leverage activity: actually prioritizing your work.

It’s far easier and more efficient to compile a small number of goals that are important to complete, pick initial tasks toward those goals, and then make a pairwise comparison between what you’re currently doing and what else is on your to-do list. Ask yourself on a recurring basis: Is there something else I could be doing that’s higher-leverage? If not, continue on your current path. If yes, it’s time to rethink what you’re doing.

# Focus on What Directly Produces Value

**T**he first heuristic for prioritizing high-leverage activities is to focus on what directly produces value. At the end of the day (or when it comes time for performance reviews), what matters is how much value you’ve created. That value is measured in terms of products shipped, users acquired, business metrics moved, or sales made, rather than in terms of hours worked, tasks completed, lines of code written, or meetings attended.

Focus on tasks that directly bring a product closer to launch; that directly increase the number of users, customers or sales; or that directly impact the core business metric your team is responsible for. Write code for a necessary product feature, tackle roadblocks or secure necessary approvals that would hold back a launch, ensure your team members are working on the right tasks, address high-priority support issues, or do anything else that leads to results.

Make sure that the effort you invest is proportional to its expected impact.

After you ship a change that produces value, find the next task that will produce value. Prioritize the ones that produce the most value with the least amount of effort. Once you do this a few times, it becomes easier to recognize which tasks are the most valuable.

The corollary to focusing on the activities that directly produce value is to defer and ignore the ones that don’t. You only have a finite amount of time.

**Don’t try to get everything done. Focus on what matters—and what matters is what produces value.**

# Focus on the Important and Non-Urgent

Along with prioritizing the activities that directly produce value, we also need to prioritize the investments that increase our ability to be more effective and deliver more value in the future. Stated simply, the second heuristic is to focus on important and non-urgent activities.

![focus-chart.jpg](image/focus-chart.jpg ':size=1000')

Quadrant 2 activities include planning our career goals, building stronger relationships, reading books and articles for professional development, adopting new productivity and efficiency habits, building tools to improve our workflows, investing in useful abstractions, ensuring that infrastructure will continue to scale, learning new programming languages, speaking at conferences, and mentoring our teammates to help them be more productive.

Carve out time to invest in skills development. Their productivity might slow down at first, but with time, the new tools and workflows that they learn will increase their effectiveness and easily compensate for the initial loss.

Be wary if you’re spending too much time on Quadrant 1’s important and urgent activities. A pager duty alert, a high-priority bug, a pressing deadline for a project, or any other type of firefighting all may be important and urgent, but assess whether you’re simply addressing the symptoms of the problem and not its underlying cause. Oftentimes, the root cause is an underinvestment in a Quadrant 2 activity.

# Protect Your Maker’s Schedule

Engineers need longer and more contiguous blocks of time to be productive than many other professionals. Productivity increases when we can maintain periods of what psychologist Mihály Csíkszentmihályi calls flow, described by people who experience it as “a state of effortless concentration so deep that they lose their sense of time, of themselves, of their problems.”

Learn to say no to unimportant activities, such as meetings that don’t require your attendance and other low-priority commitments that might fragment your schedule. Protect your time and your maker’s schedule.

# Limit the Amount of Work in Progress

After prioritizing our tasks and blocking off contiguous chunks of time, it can be tempting to try to tackle many things at once. When we fragment our attention too much, however, we end up reducing our overall productivity and hindering our ability to make substantive progress on any one thing.

**Constant context switching hinders deep engagement in any one activity and reduces our overall chance of success.**

The number of projects that you can work on simultaneously varies from person to person. Use trial and error to figure out how many projects you can work on before quality and momentum drop, and resist the urge to work on too many projects at once.

# Fight Procrastination with If-Then Plans

Sometimes, what hinders focus isn’t a lack of contiguous time or too much context switching. Instead, many people do not have sufficient motivation to summon the activation energy required to start a difficult task.

the if-then plan, in which we identify ahead of time a situation where we plan to do a certain task. Possible scenarios could be “if it’s after my 3pm meeting, then I’ll investigate this long-standing bug,” or “if it’s right after dinner, then I’ll watch a lecture on Android development.”

Subconscious followup is important because procrastination primarily stems from a reluctance to expend the initial activation energy on a task. This reluctance leads us to rationalize why it might be better to do something easier or more enjoyable, even if it has lower leverage. When we’re in the moment, the short-term value that we get from procrastinating can often dominate our decision-making process. But when we make if-then plans and decide what to do ahead of time, we’re more likely to consider the long-term benefits associated with a task.

Think about how much more effective you’d be if you could double the likelihood of completing something important you’ve been procrastinating on—whether it’s picking up a new language, reading that book on your shelf, or something else. Make an if-then plan to do it.

# Make a Routine of Prioritization

Every productivity guru recommends a different set of workflow mechanics.

- David Allen, in Getting Things Done, suggests grouping to-do items by location-based contexts and then handling tasks based on your current context.
- Tonianne DeMaria Barry and Jim Benson, in Personal Kanban, recommend creating a backlog of your work, transitioning tasks through various columns on a board (e.g., “backlog,” “ready,” “in progress,” and “done”), and limiting the amount of work-in-progress according to your own bandwidth as determined by trial and error.
- In The Pomodoro Technique, Francesco Cirillo uses a timer to track 25-minute focused sessions, called pomodoros; he works on a single task during each session.
- Nick Cern, author of Todoodlist, is an advocate of the old-fashioned pencil and paper to track what you need to get done.

**H**ave a “Current Priorities” project that I use to track the tasks that I want to accomplish in the current week. If it’s the beginning of the week, then I add to the project the tasks that I want to accomplish during that week, pulling either from my backlog or from any unfinished work from the previous week. I prioritize tasks that directly produce value for whatever projects I’m working on and also some longer-term investments that I deem important. Because optimizing for learning is important and non-urgent, I generally include some tasks related to learning something new. Some of my current priorities include writing one thousand words per day on this book, learning about self-publishing, and making daily progress on tutorials for mobile development.

An effective way to ensure that this morning prioritization happens is to make it part of your daily routine.

Find some system that helps you support a habit of prioritizing regularly. This will allow you to reflect on whether you’re spending time on your highest-leverage activities.

When you have certain personal or professional goals that you want to achieve, you’ll find that prioritization has very high leverage. You’ll see its outsized impact on your ability to get the right things done. And as you get more effective at it, you’ll feel incentivized to prioritize more regularly.

# Key Takeaways

- Write down and review to-dos. Spend your mental energy on prioritizing and processing your tasks rather than on trying to remember them. Treat your brain as a processor, not as a memory bank.
- Work on what directly leads to value. Don’t try to do everything. Regularly ask yourself if there’s something higher-leverage that you could be doing.
- Work on the important and non-urgent. Prioritize long-term investments that increase your effectiveness, even if they don’t have a deadline.
- Reduce context switches. Protect large blocks of time for your creative output, and limit the number of ongoing projects so that you don’t spend your cognitive energy actively juggling tasks.
- Make if-then plans to combat procrastination. Binding an intention to do something to a trigger significantly increases the likelihood that you’ll get it done.
- Make prioritization a habit. Experiment to figure out a good workflow. Prioritize regularly, and it’ll get easier to focus on and complete your highest-leverage activities
