What makes an effective engineer?

They’re the people who get things done. They’re the ones who ship products that users love, launch features that customers pay for, build tools that boost team productivity, and deploy systems that help companies scale. Effective engineers produce results.

Efficiency alone doesn’t guarantee effectiveness, however. An engineer who efficiently builds infrastructure that can scale to millions of requests for an internal tool that would be used by at most a hundred people isn’t effective. Nor is someone who builds a feature that only 0.1% of users adopt, when other features could reach 10% adoption—unless that 0.1% generates disproportionately more business value. Effective engineers focus on value and impact—they know how to choose which results to deliver.

An effective engineer, therefore, is defined by the rate at which he or she produces value per unit of time worked.

Part 1: Adopt the Right Mindsets
Part 2: Execute, Execute, Execute
