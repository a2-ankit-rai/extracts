Focusing on small, incremental changes also opens the door to new development techniques that aren’t possible in traditional release workflows. Suppose that in a product discussion, we’re debating whether we should keep a certain feature. Rather than letting opinions and politics dictate the feature’s importance or waiting for the next release cycle to start gathering usage data, we could simply log the interaction we care about, deploy it, and start seeing the initial data trickle in within minutes. Or suppose we see a performance regression on one of our web pages. Rather than scanning through the code looking for regressions, we can spend a few minutes deploying a change to enable logging so that we can get a live breakdown of where time is being spent.

# Move Fast to Learn Fast

The faster that you can iterate, the more that you can learn about what works and what doesn’t work. You can build more things and try out more ideas. Not every change will produce positive value and growth, of course.

Continuous deployment is but one of many powerful tools at your disposal for increasing iteration speed. Other options include investing in time-saving tools, improving your debugging loops, mastering your programming workflows, and, more generally, removing any bottlenecks that you identify.

# Invest in Time-Saving Tools

If you have to do something manually more than twice, then write a tool for the third time. There are only a finite number of work hours in the day, so increasing your effort as a way to increase your impact fails to scale. Tools are the multipliers that allow you to scale your impact beyond the confines of the work day.

Two additional effects make investing in time-saving tools approach even more compelling.

- First, faster tools get used more often. If the only option for travel from San Francisco to New York was a week-long train ride, we wouldn’t make the trip very often; but since the advent of passenger airlines in the 1950s, people can now make the trip multiple times per year. Similarly, when a tool halves the time it takes to complete a 20-minute activity that we perform 3 times a day, we save much more than 30 minutes per day—because we tend to use it more often.
- Second, faster tools can enable new development workflows that previously weren’t possible.

Sometimes, the time-saving tool that you built might be objectively superior to the existing one, but the switching costs discourage other engineers from actually changing their workflow and learning your tool. In these situations, it’s worth investing the additional effort to lower the switching cost and to find a smoother way to integrate the tool into existing workflows. Perhaps you can enable other engineers to switch to the new behavior with only a small configuration change.

At work, we can easily fall into an endless cycle of hitting the next deadline: getting the next thing done, shipping the next new feature, clearing the next bug in our backlog, and responding to the next issue in the never-ending stream of customer requests. We might have ideas for tools we could build to make our lives a bit easier, but the long-term value of those tools is hard to quantify. On the other hand, the short-term costs of a slipped deadline or a product manager breathing down our necks and asking when something will get done are fairly concrete.

So start small. Find an area where a tool could save time, build it, and demonstrate its value. You’ll earn leeway to explore more ambitious avenues, and you’ll find the tools you build empowering you to be more effective on future tasks. Don’t let the pressure to constantly ship new features cause the important but non-urgent task of building time-saving tools to fall by the wayside.

# Shorten Your Debugging and Validation Loops

It’s wishful thinking to believe that all the code we write will be bug-free and work the first time. In actuality, much of our engineering time is spent either debugging issues or validating that what we’re building behaves as expected. The sooner we internalize this reality, the sooner we will start to consciously invest in our iteration speed in debugging and validation loops.

As engineers, we can shortcut around normal system behaviors and user interactions when we’re testing our products. With some effort, we can programmatically build much simpler custom workflows.

When you’re fully engaged with a bug you’re testing or a new feature you’re building, the last thing you want to do is to add more work. When you’re already using a workflow that works, albeit with a few extra steps, it’s easy to get complacent and not expend the mental cycles on devising a shorter one. Don’t fall into this trap! The extra investment in setting up a minimal debugging workflow can help you fix an annoying bug sooner and with less headache.

Effective engineers know that debugging is a large part of software development. Almost instinctively, they know when to make upfront investments to shorten their debugging loops rather than pay a tax on their time for every iteration. That instinct comes from being mindful of what steps they’re taking to reproduce issues and reflecting on which steps they might be able to short circuit.

The next time you find yourself repeatedly going through the same motions when you’re fixing a bug or iterating on a feature, pause. Take a moment to think through whether you might be able to tighten that testing loop. It could save you time in the long run.

# Master Your Programming Environment

Fine-tuning the efficiency of simple actions and saving a second here or there may not seem worth it at first glance. It requires an upfront investment, and you’ll very likely be slower the first few times you try out a new and unfamiliar workflow. But consider that you’ll repeat those actions at least tens of thousands of times during your career: minor improvements easily compound over time.

**Mastery is a process, not an event, and as you get more comfortable, the time savings will start to build. The key is to be mindful of which of your common, everyday actions slow you down, and then figure out how to perform those actions more efficiently.**

Some ways you can get started on mastering your programming fundamentals:

- Get proficient with your favorite text editor or IDE. Run a Google search on productivity tips for your programming environment. Figure out the workflows for efficient file navigation, search and replace, auto-completion, and other common tasks for manipulating text and files. Learn and practice them.
- Learn at least one productive, high-level programming language. Scripting languages work wonders in comparison to compiled languages when you need to get something done quickly. Empirically, languages like C, C++, and Java tend to be 2–3x more verbose in terms of lines of code than higher-level languages like Python and Ruby; moreover, the higher-level languages come with more powerful, built-in primitives, including list comprehensions, functional arguments, and destructuring assignment. Once you factor in the additional time needed to recover from mistakes or bugs, the absolute time differences start to compound. Each minute spent writing boilerplate code for a less productive language is a minute not spent tackling the meatier aspects of a problem.
- Get familiar with UNIX (or Windows) shell commands. Being able to manipulate and process data with basic UNIX tools instead of writing a Python or Java program can reduce the time to complete a task from minutes down to seconds. Learn basic commands like grep, sort, uniq, wc, awk, sed, xargs, and find, all of which can be piped together to execute arbitrarily powerful transformations. Read through helpful documentation in the man pages for a command if you’re not sure what it does. Pick up or bookmark some useful one-liners.
- Prefer the keyboard over the mouse. Seasoned programmers train themselves to navigate within files, launch applications, and even browse the web using their keyboards as much as possible, rather than a mouse or trackpad. This is because moving our hands back and forth from the keyboard to the mouse takes time, and, given how often we do it, provides a considerable opportunity for optimization. Many applications offer keyboard shortcuts for common tasks, and most text editors and IDEs provide ways to bind custom key sequences to special actions for this purpose.
- Automate your manual workflows. Developing the skills to automate takes time, whether they be using shell scripts, browser extensions, or something else. But the cost of mastering these skills gets smaller the more often you do it and the better you get at it. As a rule of thumb, once I’ve manually performed a task three or more times, I start thinking about whether it would be worthwhile to automate it.
- Test out ideas on an interactive interpreter. In many traditional languages like C, C++, and Java, testing the behavior of even a small expression requires you to compile a program and run it. Languages like Python, Ruby, and JavaScript, however, have interpreters available allowing you to evaluate and test out expressions. Using them to build confidence that your program behaves as expected will provide a significant boost in iteration speed.
- Make it fast and easy to run just the unit tests associated with your current changes. Use testing tools that run only the subset of tests affected by your code. Even better, integrate the tool with your text editor or IDE so that you can invoke them with a few keystrokes. In general, the faster that you can run your tests, both in terms of how long it takes to invoke the tests and how long they take to run, the more you’ll use tests as a normal part of your development—and the more time you’ll save.

Given how much time we spend within our programming environments, mastering the basic tools that we use multiple times per day is a high-leverage investment. It lets us shift our limited time from the mechanics of programming to more important problems.

# Don’t Ignore Your Non-Engineering Bottlenecks

Effective engineers identify and tackle the biggest bottlenecks, even if those bottlenecks don’t involve writing code or fall within their comfort zone. They proactively try to fix processes inside their sphere of influence, and they do their best to work around areas outside of their control.

Communication is critical for making progress on people-related bottlenecks. Ask for updates and commitments from team members at meetings or daily stand-ups. Periodically check in with that product manager to make sure what you need hasn’t gotten dropped. Follow up with written communication (email or meeting notes) on key action items and dates that were decided in-person.

Even if resource constraints preclude the dependency that you want from being delivered any sooner, clarifying priorities and expectations enables you to plan ahead and work through alternatives. You might decide, for example, to handle the project dependency yourself; even though it will take additional time to learn how to do it, it will enable you to launch your feature sooner. This is a hard decision to make unless you’ve communicated regularly with the other party.

**Plan on getting feedback in an informal way. Probably over lunch or coffee or casually meeting them. Talk more often. Start with casual topics and filler words & then communicate your plans & get feedback.**

Don’t wait until after you’ve invested massive amounts of engineering time to seek final project approval. Instead, prioritize building prototypes, collecting early data, conducting user studies, or whatever else is necessary to get preliminary project approval. Explicitly ask the decision makers what they care about the most, so that you can make sure to get those details right. If meeting with the decision makers isn’t possible, talk with the product managers, designers, or other leaders who have worked closely with them and who might be able to provide insight into their thought processes.

A third type of bottleneck is the review processes that accompany any project launch, whether they be verification by the quality assurance team, a scalability or reliability review by the performance team, or an audit by the security team.

Plan ahead. Expend slightly more effort in coordination; it could make a significant dent in your iteration speed. Get the ball rolling on the requirements in your launch checklist, and don’t wait until the last minute to schedule necessary reviews. Again, communication is key to ensure that review processes don’t become bottlenecks.

Find out where the biggest bottlenecks in your iteration cycle are, whether they’re in the engineering tools, cross-team dependencies, approvals from decision-makers, or organizational processes. Then, work to optimize them.

# Key Takeaways

- The faster you can iterate, the more you can learn. Conversely, when you move too slowly trying to avoid mistakes, you lose opportunities.
- Invest in tooling. Faster compile times, faster deployment cycles, and faster turnaround times for development all provide time-saving benefits that compound the more you use them.
- Optimize your debugging workflow. Don’t underestimate how much time gets spent validating that your code works. Invest enough time to shorten those workflows.
- Master the fundamentals of your craft. Get comfortable and efficient with the development environment that you use on a daily basis. This will pay off dividends throughout your career.
- Take a holistic view of your iteration loop. Don’t ignore any organizational and team-related bottlenecks that may be within your circle of influence.
