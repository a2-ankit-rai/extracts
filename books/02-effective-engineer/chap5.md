# Use Metrics to Drive Progress

Good metrics accomplish a number of goals. First, they help you focus on the right things. They confirm that your product changes—and all the effort you’ve put into them—actually achieve your objectives.

Define a metric associated with your goal—whether that metric is weekly active rate, response time, click-through rate, or something else—and then measure the change’s impact. Without those measurements, we’re only able to proceed based on intuition; and we have few avenues for understanding whether our intuition is correct.

Second, when visualized over time, good metrics help guard against future regressions. Engineers know the value of writing a regression test while fixing bugs: it confirms that a patch actually fixes a bug and detects if the bug re-surfaces in the future. Good metrics play a similar role, but on a system-wide scale.

Third, good metrics can drive forward progress. The practice ensures that performance trends in the right direction.

Fourth, a good metric lets you measure your effectiveness over time and compare the leverage of what you’re doing against other activities you could be doing instead.

Given the benefits of good metrics, it’s worth asking yourself:

- Is there some way to measure the progress of what I’m doing?
- If a task I’m working on doesn’t move a core metric, is it worth doing? Or is there a missing key metric?

# Pick the Right Metric to Incentivize the Behavior You Want

**Hours worked per week vs. productivity per week.** It is much more reasonable to align your metric with productivity per week, where productivity in your focus area is measured by factors like product quality, site speed, or user growth.

**Average response times vs. 95th or 99th percentile response times.** Focusing on the average response time leads to a very different set of priorities than focusing on the highest 95th or 99th percentile of response times. To decrease the average, you’ll focus more on general infrastructure improvements that can shave off milliseconds from all requests. The average is the right metric to use if your goal is to reduce server costs by cutting down aggregate computation time. To decrease the 95th or 99th percentile, however, you’ll need to hunt down the worst-case behaviors in your system. In this case, it’s important to focus on the slowest responses because they tend to reflect the experiences of your power users—users who have the most data and the most activity and who tend to be more computationally expensive to support.

**Registered users vs. weekly growth rate of registered users.** Measuring growth in terms of your weekly growth rate (for example, the ratio of new registered users in a week over total registered users), shows whether growth is slowing down.

**Weekly active users vs. weekly active rate by age of cohort.** When tracking user engagement, the number of weekly active users doesn’t provide a complete picture. An alternative and more accurate metric would be the weekly active rate by age of cohort. In other words, measure the fraction of users who are still weekly actives the nth week after signing up, and track how that number changes over time. This metric provides more actionable insight into how product changes have affected the engagement of newer cohorts of users as compared to older ones.

The more complex the product and the goal, the more options there are for what to measure and not to measure, and the more flexibility there is to guide where effort gets spent and what output gets produced. When deciding which metrics to use, choose ones that 1) maximize impact, 2) are actionable, and 3) are responsive yet robust. Look for a metric that, when optimized, maximizes impact for the team.

Having a single, unifying metric—whether it’s products sold, rentals booked, content generated, or something else—enables you to compare the output of disparate projects and helps your team decide how to handle externalities. For example, should a performance team cut a product feature to improve page load times? That decision might be a yes if they’re just optimizing a site speed metric, but it will be more nuanced (and more likely aligned with the company’s desired impact) if they’re optimizing a higher-level product metric.

An actionable metric is one whose movements can be causally explained by the team’s efforts, include things like signup conversion rate, or the percentage of registered users that are active weekly over time.

A responsive metric updates quickly enough to give feedback about whether a given change was positive or negative, so that your team can learn where to apply future efforts. It is a leading indicator of how your team is currently doing.

A metric that measures active users in the past week is more responsive than one that tracks active users in the past month, since the latter requires a month after any change to fully capture the effects. However, a metric also needs to be robust enough that external factors outside of the team’s control don’t lead to significant noise. Trying to track performance improvements with per-minute response time metrics would be difficult because of their high variance. However, tracking the response times averaged over an hour or a day would make the metric more robust to noise and allow trends to be detected more easily. Responsiveness needs to be balanced with robustness.

Because the choice of metric can have such a significant impact on behavior, it’s a powerful leverage point. Dedicate the time to pick the right metric, whether it’s just for yourself or for your team.

# Instrument Everything to Understand What’s Going On

When establishing our goals, it’s important to choose carefully what core metrics to measure (or not measure) and optimize. When it comes to day-to-day operations, however, you should be less discriminatory: measure and instrument as much as possible. Although these two principles may seem contradictory, they actually complement each other. The first describes a high-level, big-picture activity, whereas the second is about gaining insight into what’s going on with systems that we’ve built.

Adopting a mindset of instrumentation means ensuring we have a set of dashboards that surface key health metrics and that enable us to drill down to the relevant data. However, many of the questions we want to answer tend to be exploratory, since we often don’t know everything that we want to measure ahead of time. Therefore, we need to build flexible tools and abstractions that make it easy to track additional metrics.

You don’t have to be a large engineering team operating at scale to start instrumenting your systems. Open-source tools like Graphite, StatsD, InfluxDB, Ganglia, Nagios, and Munin make it easy to monitor systems in near real-time. Teams who want a managed, enterprise solution have options like New Relic or AppDynamics that can quickly provide code-level performance visibility into many standard platforms. Given how much insight instrumentation can provide, how can you afford not to prioritize it?

# Internalize Useful Numbers

Measuring the goals you want to achieve and instrumenting the systems that you want to understand are high-leverage activities. They both take some upfront work, but their long-term payoffs are high. Oftentimes, however, you don’t need accurate numbers to make effective decisions; you just need ones that are in the right ballpark. Ensuring you have access to a few useful numbers to approximate your progress and benchmark your performance is a high-leverage investment: they provide the benefits of metrics at a much lower cost.

![latency-numbers.jpg](./image/latency-numbers.jpg)

The small amount of upfront work it takes to accumulate all this information gives you valuable rules of thumb that you can apply in the future. To obtain performance-related numbers, you can write small benchmarks to gather data you need. For example, write a small program that profiles the common operations you do on your key building blocks and subsystems. Other numbers may require more research, like talking with teams (possibly at other companies) that have worked in similar focus areas, digging through your own historical data, or measuring parts of the data yourself.

When you find yourself wondering which of several designs might be more performant, whether a number is in the right ballpark, how much better a feature could be doing, or whether a metric is behaving normally, pause for a moment. Think about whether these are recurring questions and whether some useful numbers or benchmarks might be helpful for answering them. If so, spend some time gathering and internalizing that data.

# Be Skeptical about Data Integrity

When it comes to metrics, compare the numbers with your intuition to see if they align. Try to arrive at the same data from a different direction and see if the metrics still make sense. If a metric implies some other property, try to measure the other property to make sure the conclusions are consistent.

Given the importance of metrics, investing the effort to ensure that your data is accurate is high-leverage. Here are some strategies that you can use to increase confidence in your data integrity:

- **Log data liberally, in case it turns out to be useful later on.** Eric Colson, former VP of Data Science and Engineering at Netflix, explained that Netflix throws reams of semi-structured logs into a scalable data store called Cassandra, and decides later on whether that data might be useful for analysis. 44
- **Build tools to iterate on data accuracy sooner.** Real-time analytics address this issue, as do tools that visualize collected data during development. When I worked on the experiment and analytics frameworks at Quora, we built tools to easily inspect what was being logged by each interaction. 45 This paid off huge dividends.
- **Write end-to-end integration tests to validate your entire analytics pipeline.** These tests may be time-consuming to write. Ultimately, however, they will help increase confidence in your data integrity and also protect against future changes that might introduce inaccuracies.
- **Examine collected data sooner.** Even if you need to wait weeks or months to have enough data for a meaningful analysis, check the data sooner to ensure that a sufficient amount was logged correctly. Treat data measurement and analysis as parts of the product development workflow rather than as activities to be bolted on afterwards.
- **Cross-validate data accuracy by computing the same metric in multiple ways.** This is a great way to sanity check that the number is in the right ballpark.
- **When a number does look off, dig in to it early. Understand what’s going on.** Figure out whether the discrepancy is due to a bug, a misinterpretation, or something else.

Make sure your data is reliable. The only thing worse than having no data is the illusion of having the right data.

# Key Takeaways

- **Measure your progress.** It’s hard to improve what you don’t measure. How would you know what types of effort are well spent?
- **Carefully choose your top-level metric.** Different metrics incentivize different behaviors. Figure out which behaviors you want.
- **Instrument your system.** The higher your system’s complexity, the more you need instrumentation to ensure that you’re not flying blind. The easier it is to instrument more metrics, the more often you’ll do it.
- **Know your numbers.** Memorize or have easy access to numbers that can benchmark your progress or help with back-of-the-envelope calculations.
- **Prioritize data integrity.** Having bad data is worse than having no data, because you’ll make the wrong decisions thinking that you’re right.
