Use Leverage as Your Yardstick for Effectiveness: given the hundreds or more tasks that we all could be doing in our jobs right now, how should we decide what to actually work on in order to more effectively achieve our goals?

Prioritizing these different activities is assessing their leverage. Leverage is defined by a simple equation. It’s the value, or impact, produced per time invested:

Put another way, leverage is the return on investment (ROI) for the effort that’s put in. Effective engineers aren’t the ones trying to get more things done by working more hours. They’re the ones who get things done efficiently—and who focus their limited time on the tasks that produce the most value. They try to increase the numerator in that equation while keeping the denominator small. Leverage, therefore, is the yardstick for measuring how effective your activities are.

Your overall leverage—the amount of value that you produce per unit time—can only be increased in three ways:

- By reducing the time it takes to complete a certain activity.
- By increasing the output of a particular activity.
- By shifting to higher-leverage activities.

These three ways naturally translate into three questions we can ask ourselves about any activity we’re working on:

- How can I complete this activity in a shorter amount of time?
- How can I increase the value produced by this activity?
- Is there something else that I could spend my time on that would produce more value?

To increase the leverage of each activity, ask yourself the previous three questions, each of which leads to a different avenue of potential improvements.

For example, you might have a one-hour meeting that you’ve scheduled with your team to review their progress on a project. You can increase the meeting’s leverage by:

1. Defaulting to a half-hour meeting instead of a one-hour meeting to get the same amount done in less time.

2. Preparing an agenda and a set of goals for the meeting and circulating them to attendees beforehand so that the meeting is focused and more productive.

3. If an in-person discussion isn’t actually necessary, replacing the meeting with a email discussion and spending the time building an important feature instead.

You might increase the leverage of your development time by:

1. Automating parts of the development or testing process that have thus far been done manually, so that you can iterate more quickly.

2. Prioritizing tasks based on how critical they are for launch so that you maximize the value of what you finally ship.

3. Talking with the customer support team to gain insights into the customers’ biggest pain points, and using that knowledge to understand whether there’s another feature you could be working on that would produce even more value with less development effort.

Suppose that you’re a performance engineer identifying and fixing the bottlenecks in a web application. Some approaches you might consider to increase your leverage include:

1. Learning to effectively use a profiling tool so that you can reduce the time it takes to identify each bottleneck.

2. Measuring both the performance and visit frequency of each web page so that you can address the bottlenecks that affect the most traffic first, thereby increasing the impact of each fix you make.

3. Working with product teams to design performant software from the outset, so that application speed is prioritized as a feature during product development rather than treated as a bug to be fixed.

**When you successfully shorten the time required for an activity, increase its impact, or shift to a higher-leverage activity, you become a more effective engineer.**

# Key Takeaways

- Use leverage to measure your engineering effectiveness

. Focus on what generates the highest return on investment for your time spent.

- Systematically increase the leverage of your time.

Find ways to get an activity done more quickly, to increase the impact of an activity, or to shift to activities with higher leverage.

- Focus your effort on leverage points.

Time is your most limited asset. Identify the habits that produce disproportionately high impact for the time you invest.
