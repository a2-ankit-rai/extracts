
The Daily Stoic

Book by Ryan Holiday

The Daily Stoic is an original translation of selections from several stoic philosophers including Epictetus, Marcus Aurelius, Seneca, Musonius Rufus, Zeno and others. It aims to provide lessons about personal growth, life management and practicing mindfulness.
