**WHERE PHILOSOPHY BEGINS**

“An important place to begin in philosophy is this: a clear perception of one’s own ruling principle.”

We begin our journey into philosophy when we become aware of the ability to analyze our own minds.

**ACCURATE SELF-ASSESSMENT**

We underestimate our capabilities just as much and just as dangerously as we overestimate other abilities. Cultivate the ability to judge yourself accurately and honestly. Look inward to discern what you’re capable of and what it will take to unlock that potential.

**AWARENESS IS FREEDOM**

Take an inventory of your obligations from time to time. How many of these are self-imposed? How many of them are truly necessary? Are you as free as you think?

**CUTTING BACK ON THE COSTLY**

Remember: even what we get for free has a cost, if only in what we pay to store it—in our garages and in our minds. As you walk past your possessions today, ask yourself: Do I need this? Is it superfluous? What’s this actually worth? What is it costing me?

You might be surprised by the answers and how much we’ve been paying without even knowing it.

**DON’T TELL YOURSELF STORIES**

“In public avoid talking often and excessively about your accomplishments and dangers, for however much you enjoy recounting your dangers, it’s not so pleasant for others to hear about your affairs.”

Try your best not to create this fantasy bubble—live in what’s real. Listen and connect with people, don’t perform for them.

**DON’T TRUST THE SENSES**

Self-awareness is the ability to objectively evaluate the self. It’s the ability to question our own instincts, patterns, and assumptions. Oiêsis, self-deception or arrogant and unchallenged opinion, requires that we hold all our opinions up to hard scrutiny; even our eyes deceive us.

Because our senses are often wrong, our emotions overly alarmed, our projections overly optimistic, we’re better off not rushing into conclusions about anything. We can take a beat with everything we do and become aware of everything that’s going on so we can make the right decision.

**DON’T UNINTENTIONALLY HAND OVER YOUR FREEDOM**

“If a person gave away your body to some passerby, you’d be furious. Yet you hand over your mind to anyone who comes along, so they may abuse you, leaving it disturbed and troubled—have you no shame in that?”

To the Stoics, this is an abomination. They know that the world can control our bodies—we can be thrown in jail or be tossed about by the weather. But the mind? That’s ours. We must protect it. Maintain control over your mind and perceptions, they’d say. It’s your most prized possession.

**Control your mind. Take charge of it . Don't get your mind easily influenced by anything . Subject everything you observe to scrutiny.**

**FIND THE RIGHT SCENE**

Consciously consider whom you allow into your life—not like some snobby elitist but like someone who is trying to cultivate the best life possible. Ask yourself about the people you meet and spend time with: Are they making me better? Do they encourage me to push forward and hold me accountable? Or do they drag me down to their level? Now, with this in mind, ask the most important question: Should I spend more or less time with these folks?

**FIND YOURSELF A CATO**

Seneca tells us that we should each have our own Cato—a great and noble person we can allow into our minds and use to guide our actions, even when they’re not physically present. The economist Adam Smith had a similar concept, which he called the indifferent spectator. It doesn’t have to be an actual person, just someone who, like Seneca said, can stand witness to our behavior. Someone who can quietly admonish us if we are considering doing something lazy, dishonest, or selfish.

**ONE DAY IT WILL ALL MAKE SENSE**

This sense of being wronged is a simple awareness problem. We need to remember that all things are guided by reason—but that it is a vast and universal reason that we cannot always see. That the surprise hurricane was the result of a butterfly flapping its wings a hemisphere away or that misfortune we have experienced is simply the prelude to a pleasant and enviable future.

“Whenever you find yourself blaming providence, turn it around in your mind and you will see that what has happened is in keeping with reason.”

**SELF-DECEPTION IS OUR ENEMY**

As Epictetus put it, “It is impossible for a person to begin to learn what he thinks he already knows.” Today, we will be unable to improve, unable to learn, unable to earn the respect of others if we think we’re already perfect, a genius admired far and wide. In this sense, ego and self-deception are the enemies of the things we wish to have because we delude ourselves into believing that we already possess them.

**THE PRESENT IS ALL WE POSSESS**

Today, notice how often you look for more. That is, wanting the past to be more than what it was (different, better, still here, etc.) or wanting the future to unfold exactly as you expect (with hardly a thought as to how that might affect other people).

“Yesterday’s the past, tomorrow’s the future, but today is a gift. That’s why it’s called the present.” This present is in our possession—but it has an expiration date, a quickly approaching one. If you enjoy all of it, it will be enough. It can last a whole lifetime.

**THAT SACRED PART OF YOU**

Take a little time today to remember that you’re blessed with the capacity to use logic and reason to navigate situations and circumstances. This gives you unthinkable power to alter your circumstances and the circumstances of others. And remember that with power comes responsibility.

**THE BEAUTY OF CHOICE**

“You are not your body and hair-style, but your capacity for choosing well. If your choices are beautiful, so too will you be.”

You might look beautiful today, but if that was the result of vain obsession in the mirror this morning, the Stoics would ask, are you actually beautiful? A body built from hard work is admirable. A body built to impress gym rats is not.

That’s what the Stoics urge us to consider. Not how things appear, but what effort, activity, and choices they are a result of.

**IMPOSSIBLE WITHOUT YOUR CONSENT**

On tough days we might say, “My work is overwhelming,” or “My boss is really frustrating.” If only we could understand that this is impossible. Someone can’t frustrate you, work can’t overwhelm you—these are external objects, and they have no access to your mind. Those emotions you feel, as real as they are, come from the inside, not the outside.

What we assume, what we willingly generate in our mind, that’s on us. We can’t blame other people for making us feel stressed or frustrated any more than we can blame them for our jealousy. The cause is within us. They’re just the target.

**TIMELESS WISDOM**

Remember, each individual has a choice. You are always the one in control. The cause of irritation—or our notion that something is bad—that comes from us, from our labels or our expectations. Just as easily, we can change those labels; we can change our entitlement and decide to accept and love what’s happening around us.

**READY AND AT HOME**

We’d be crazy to want to face difficulty in life. But we’d be equally crazy to pretend that it isn’t going to happen. Which is why when it knocks on our door—as it very well may this morning—let’s make sure we’re prepared to answer. Not the way we are when a surprise visitor comes late at night, but the way we are when we’re waiting for an important guest: dressed, in the right head space, ready to go.

The point is not to wish for these adversities, but for the virtue that makes adversities bearable.

**THE BEST RETREAT IS IN HERE, NOT OUT THERE**

We can find a retreat at any time by looking inward. We can sit with our eyes closed and feel our breath go in and out. We can turn on some music and tune out the world. We can turn off technology or shut off those rampant thoughts in our head. That will provide us peace. Nothing else.

**THE SIGN OF TRUE EDUCATION**

“What is it then to be properly educated? It is learning to apply our natural preconceptions to the right things according to Nature, and beyond that to separate the things that lie within our power from those that don’t.”

**THERE IS PHILOSOPHY IN EVERYTHING**

This is what Epictetus means about the study of philosophy. Study, yes, but go live your life as well. It’s the only way that you’ll actually understand what any of it means. And more important, it’s only from your actions and choices over time that it will be possible to see whether you took any of the teachings to heart.

**WEALTH AND FREEDOM ARE FREE**

There are two ways to be wealthy—to get everything you want or to want everything you have. Which is easier right here and right now? The same goes for freedom. If you chafe and fight and struggle for more, you will never be free. If you could find and focus on the pockets of freedom you already have? Well, then you’d be free right here, right now.

“. . . freedom isn’t secured by filling up on your heart’s desire but by removing your desire.”

**WHAT RULES YOUR RULING REASON?**

Don't stop at Stoicism, but explore the forces that drive and make Stoicism possible. Learn what underpins this philosophy you’re studying, how the body and mind tick. Understand not only your ruling reason—the watchmen—but whoever and whatever rules that too.

**PAY WHAT THINGS ARE WORTH**

The good things in life cost what they cost. The unnecessary things are not worth it at any price. The key is being aware of the difference.

**COWARDICE AS A DESIGN PROBLEM**

It’s the chaos that ensues from not having a plan. Not because plans are perfect, but because people without plans—like a line of infantrymen without a strong leader—are much more likely to get overwhelmed and fall apart.

Don’t try to make it up on the fly. Have a plan.

**WHY DO YOU NEED TO IMPRESS THESE PEOPLE AGAIN?**

The irony, as Marcus Aurelius points out repeatedly, is that the people whose opinion we covet are not all that great. They’re flawed—they’re distracted and wowed by all sorts of silly things themselves. We know this and yet we don’t want to think about it. To quote Fight Club again, “We buy things we don’t need, to impress people we don’t like.”

**REASON IN ALL THINGS**

If our lives are not ruled by reason, what are they ruled by? Impulse? Whim? Mimicry? Unthinking habit? As we examine our past behavior, it’s sad how often we find this to be the case—that we were not acting consciously or deliberately but instead by forces we did not bother to evaluate. It also happens that these are the instances that we’re mostly likely to regret.

**YOU’RE A PRODUCT OF YOUR TRAINING**

You must put in place training and habits now to replace ignorance and ill discipline. Only then will you begin to behave and act differently. Only then will you stop seeking the impossible, the shortsighted, and the unnecessary.
