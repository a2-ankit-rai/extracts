## CONTROL AND CHOICE

“The chief task in life is simply this: to identify and separate matters so that I can say clearly to myself which are externals not under my control, and which have to do with the choices I actually control. Where then do I look for good and evil? Not to uncontrollable externals, but within myself to the choices that are my own . . .”

If we can focus on making clear what parts of our day are within our control and what parts are not, we will not only be happier, we will have a distinct advantage over other people who fail to realize they are fighting an unwinnable battle.

## EDUCATION IS FREEDOM

We should not trust the masses who say only the free can be educated, but rather the lovers of wisdom who say that only the educated are free.

Remember that imperative on the days you start to feel distracted, when watching television or having a snack seems like a better use of your time than reading or studying philosophy. Knowledge—self-knowledge in particular—is freedom.

## BE RUTHLESS TO THE THINGS THAT DON’T MATTER

One of the hardest things to do in life is to say “No.” To invitations, to requests, to obligations, to the stuff that everyone else is doing. Even harder is saying no to certain time-consuming emotions: anger, excitement, distraction, obsession, lust. None of these impulses feels like a big deal by itself, but run amok, they become a commitment like anything else.

It may hurt some feelings. It may turn people off. It may take some hard work.But the more you say no to the things that don’t matter, the more you can say yes to the things that do. This will let you live and enjoy your life—the life that you want.

## THE BIG THREE

The following little reminder sums up the three most essential parts of Stoic philosophy worth carrying with you every day, into every decision:

- Control your perceptions.
- Direct your actions properly.
- Willingly accept what’s outside your control.

## CLARIFY YOUR INTENTIONS

Let all your efforts be directed to something, let it keep that end in view. It’s not activity that disturbs people, but false conceptions of things that drive them mad.

Law 29 of The 48 Laws of Power is: Plan All The Way To The End. Robert Greene writes, “By planning to the end you will not be overwhelmed by circumstances and you will know when to stop. Gently guide fortune and help determine the future by thinking far ahead.”

Having an end in mind is no guarantee that you’ll reach it—no Stoic would tolerate that assumption—but not having an end in mind is a guarantee you won’t.

When your efforts are not directed at a cause or a purpose, how will you know what to do day in and day out? How will you know what to say no to and what to say yes to? How will you know when you’ve had enough, when you’ve reached your goal, when you’ve gotten off track, if you’ve never defined what those things are?

The answer is that you cannot. And so you are driven into failure—or worse, into madness by the oblivion of directionlessness.

## WHERE, WHO, WHAT, AND WHY

Have you taken the time to get clarity about who you are and what you stand for? Or are you too busy chasing unimportant things, mimicking the wrong influences, and following disappointing or unfulfilling or nonexistent paths?

## SEVEN CLEAR FUNCTIONS OF THE MIND

- Choice—to do and think right
- Refusal—of temptation
- Yearning—to be better
- Repulsion—of negativity, of bad influences, of what isn’t true
- Preparation—for what lies ahead or whatever may happen
- Purpose—our guiding principle and highest priority
- Assent—to be free of deception about what’s inside and outside our control (and be ready to accept the latter)

## SEEING OUR ADDICTIONS

We must give up many things to which we are addicted, considering them to be good. Otherwise, courage will vanish, which should continually test itself. Greatness of soul will be lost, which can’t stand out unless it disdains as petty what the mob regards as most desirable.

You must reclaim the ability to abstain because within it is your clarity and self-control.

## WHAT WE CONTROL AND WHAT WE DON’T

An honest understanding of what is within our control provides real clarity about the world: all we have is our own mind. Remember that today when you try to extend your reach outward—that it’s much better and more appropriately directed inward.

## IF YOU WANT TO BE STEADY

**Seek steadiness, stability, and tranquility**

it’s about filtering the outside world through the straightener of our judgment. That’s what our reason can do—it can take the crooked, confusing, and overwhelming nature of external events and make them orderly.

However, if our judgments are crooked because we don’t use reason, then everything that follows will be crooked, and we will lose our ability to steady ourselves in the chaos and rush of life. If you want to be steady, if you want clarity, proper judgment is the best way.

## IF YOU WANT TO BE UNSTEADY

Serenity and stability are results of your choices and judgment, not your environment. If you seek to avoid all disruptions to tranquility—other people, external events, stress—you will never be successful. Your problems will follow you wherever you run and hide. But if you seek to avoid the harmful and disruptive judgments that cause those problems, then you will be stable and steady wherever you happen to be.

## THE ONE PATH TO SERENITY

This morning, remind yourself of what is in your control and what’s not in your control. Remind yourself to focus on the former and not the latter.

Before lunch, remind yourself that the only thing you truly possess is your ability to make choices (and to use reason and judgment when doing so). This is the only thing that can never be taken from you completely.

In the afternoon, remind yourself that aside from the choices you make, your fate is not entirely up to you. The world is spinning and we spin along with it—whichever direction, good or bad.

In the evening, remind yourself again how much is outside of your control and where your choices begin and end.

As you lie in bed, remember that sleep is a form of surrender and trust and how easily it comes. And prepare to start the whole cycle over again tomorrow.

## CIRCLE OF CONTROL

A wise person knows what’s inside their circle of control and what is outside of it.

The circle of control contains just one thing: YOUR MIND.

## CUT THE STRINGS THAT PULL YOUR MIND

“Understand at last that you have something in you more powerful and divine than what causes the bodily passions and pulls you like a mere puppet. What thoughts now occupy my mind? Is it not fear, suspicion, desire, or something like that?”

“Man is pushed by drives but pulled by values.”

## PEACE IS IN STAYING THE COURSE

Believing in yourself and trusting that you are on the right path, and not being in doubt by following the myriad footpaths of those wandering in every direction.

Clarity of vision allows us to have this belief. That’s not to say we’re always going to be 100 percent certain of everything, or that we even should be. Rather, it’s that we can rest assured we’re heading generally in the right direction—that we don’t need to constantly compare ourselves with other people or change our mind every three seconds based on new information.

## NEVER DO ANYTHING OUT OF HABIT

We should apply the same ruthlessness to our own habits. In fact, we are studying philosophy precisely to break ourselves of rote behavior. Find what you do out of rote memory or routine. Ask yourself: Is this really the best way to do it? Know why you do what you do—do it for the right reasons.

## REBOOT THE REAL WORK

Let go of the past. We must only begin. Believe me and you will see.

As we get older, failure is not so inconsequential anymore. What’s at stake is not some arbitrary grade or intramural sports trophy, but the quality of your life and your ability to deal with the world around you.

Just begin the work. The rest follows.

## SEE THE WORLD LIKE A POET AND AN ARTIST

There is clarity (and joy) in seeing what others can’t see, in finding grace and harmony in places others overlook. Isn’t that far better than seeing the world as some dark place?

## WHEREVER YOU GO, THERE YOUR CHOICE IS

In all circumstances—adversity or advantage—we really have just one thing we need to do: focus on what is in our control as opposed to what is not.

Ultimately, this is clarity. Whoever we are, wherever we are—what matters is our choices. What are they? How will we evaluate them? How will we make the most of them? Those are the questions life asks us, regardless of our station. How will you answer?

## REIGNITE YOUR THOUGHTS

No matter what happens, no matter how disappointing our behavior has been in the past, the principles themselves remain unchanged. We can return and embrace them at any moment. What happened yesterday—what happened five minutes ago—is the past. We can reignite and restart whenever we like.

Why not do it right now?

## A MORNING RITUAL

Every day, starting today, ask yourself these same tough questions. Let philosophy and hard work guide you to better answers, one morning at a time, over the course of a life.

The idea is to take some time to look inward and examine.

“Ask yourself the following first thing in the morning:

- What am I lacking in attaining freedom from passion?
- What for tranquility?
- What am I? A mere body, estate-holder, or reputation? None of these things.
- What, then? A rational being.
- What then is demanded of me? Meditate on your actions.
- How did I steer away from serenity?
- What did I do that was unfriendly, unsocial, or uncaring?
- What did I fail to do in all these things?”

## THE DAY IN REVIEW

Keep your own journal, whether it’s saved on a computer or in a little notebook. Take time to consciously recall the events of the previous day. Be unflinching in your assessments. Notice what contributed to your happiness and what detracted from it. Write down what you’d like to work on or quotes that you like. By making the effort to record such thoughts, you’re less likely to forget them. An added bonus: you’ll have a running tally to track your progress too.

## THE TRUTH ABOUT MONEY

Money only marginally changes life. It doesn’t solve the problems that people without it seem to think it will. In fact, no material possession will. External things can’t fix internal issues.

## PUSH FOR DEEP UNDERSTANDING

I learned to read carefully and not be satisfied with a rough understanding of the whole, and not to agree too quickly with those who have a lot to say about something.

## THE ONLY PRIZE

By having some self-respect for your own mind and prizing it, you will please yourself and be in better harmony with your fellow human beings, and more in tune with the gods—praising everything they have set in order and allotted you.

The more things we desire and the more we have to do to earn or attain those achievements, the less we actually enjoy our lives—and the less free we are.

## THE POWER OF A MANTRA

A mantra can be especially helpful in the meditative process because it allows us to block out everything else while we focus.

Stoic mantra—a reminder or watch phrase to use when we feel false impressions, distractions, or the crush of everyday life upon us.

Have a mantra and use it to find the clarity you crave.

## THE THREE AREAS OF TRAINING

We must consider what we should desire and what we should be averse to. It’s not enough to just listen to your body—because our attractions often lead us astray.

Next, we must examine our impulses to act—that is, our motivations. Are we doing things for the right reasons? Or do we act because we haven’t stopped to think? Or do we believe that we have to do something?

Finally, there is our judgment. Our ability to see things clearly and properly comes when we use our great gift from nature: reason.

## WATCHING THE WISE

“Take a good hard look at people’s ruling principle, especially of the wise, what they run away from and what they seek out.”

## KEEP IT SIMPLE

Today, let’s focus just on what’s in front of us.

Approach each task as if it were your last, because it very well could be. And even if it isn’t, botching what’s right in front of you doesn’t help anything. Find clarity in the simplicity of doing your job today.

## YOU DON’T HAVE TO STAY ON TOP OF EVERYTHING

How much more time, energy, and pure brainpower would you have available if you drastically cut your media consumption? How much more rested and present would you feel if you were no longer excited and outraged by every scandal, breaking story, and potential crisis (many of which never come to pass anyway)?

## PHILOSOPHY AS MEDICINE OF THE SOUL

Stoicism is designed to be medicine for the soul. It relieves us of the vulnerabilities of modern life. It restores us with the vigor we need to thrive in life. Check in with it today, and let it do its healing.
