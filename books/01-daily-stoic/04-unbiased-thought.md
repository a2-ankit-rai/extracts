**THE COLOR OF YOUR THOUGHTS**

“Your mind will take the shape of what you frequently hold in thought, for the human spirit is colored by such impressions.”

If you hold a perpetually negative outlook, soon enough everything you encounter will seem negative. Close it off and you’ll become closed-minded. Color it with the wrong thoughts and your life will be dyed the same.

**BE WARY OF WHAT YOU LET IN**

How much harder is it to do the right thing when you’re surrounded by people with low standards? How much harder is it to be positive and empathetic inside the negativity bubble of television chatter? How much harder is it to focus on your own issues when you’re distracted with other people’s drama and conflict?

We’ll inevitably be exposed to these influences at some point, no matter how much we try to avoid them. But when we are, there is nothing that says we have to allow those influences to penetrate our minds. We have the ability to put our guard up and decide what we actually allow in. Uninvited guests might arrive at your home, but you don’t have to ask them to stay for dinner. You don’t have to let them into your mind.

**DECEIVED AND DIVIDED**

“Circumstances are what deceive us—you must be discerning in them. We embrace evil before good. We desire the opposite of what we once desired. Our prayers are at war with our prayers, our plans with our plans.”

The Stoics say that that war is usually a result of our conflicting desires, our screwed-up judgments or biased thoughts. We don’t stop and ask: OK, what do I really want? What am I actually after here? If we did, we’d notice the contradictory and inconsistent wishes that we have. And then we’d stop working against ourselves.

**DON’T LET THIS GO TO YOUR HEAD**

When we experience success, we must make sure that it doesn’t change us—that we continue to maintain our character despite the temptation not to. Reason must lead the way no matter what good fortune comes along.

**TRUST, BUT VERIFY**

We lose very little by taking a beat to consider our own thoughts. Is this really so bad? What do I really know about this person? Why do I have such strong feelings here? Is anxiety really adding much to the situation? What’s so special about __________?

We’re still free to use our instincts, but we should always, as the Russian proverb says, “trust, but verify.”

“First off, don’t let the force of the impression carry you away. Say to it, ‘hold up a bit and let me see who you are and where you are from—let me put you to the test’ . . .”

**PREPARE YOURSELF FOR NEGATIVITY**

You can be certain that at some point you are going to meet someone who will behave like a jerk & ruin your day.

You have to be ready for it.

This exercise calls to mind a joke from the eighteenth-century writer and witticist Nicolas Chamfort, who remarked that if you “swallow a toad every morning,” you’ll be fortified against anything else disgusting that might happen the rest of the day.

The point of this preparation is not to write off everyone in advance. It’s that, maybe, because you’ve prepared for it, you’ll be able to act with patience, forgiveness, and understanding.

**EXPECT TO CHANGE YOUR OPINIONS**

We must fight our biases and preconceptions: because they are a liability. Ask yourself: What haven’t I considered? Why is this thing the way it is? Am I part of the problem here or the solution? Could I be wrong here? Be doubly careful to honor what you do not know, and then set that against the knowledge you actually have.

If we ever do want to become wise, it comes from the questioning and from humility—not, as many would like to think, from certainty, mistrust, and arrogance.

**THE COST OF ACCEPTING COUNTERFEITS**

“When it comes to money, where we feel our clear interest, we have an entire art where the tester uses many means to discover the worth . . . just as we give great attention to judging things that might steer us badly. But when it comes to our own ruling principle, we yawn and doze off, accepting any appearance that flashes by without counting the cost.”

The first and greatest task of the philosopher is to test and separate appearances, and to act on nothing that is untested.

**TEST YOUR IMPRESSIONS**

“From the very beginning, make it your practice to say to every harsh impression, ‘you are an impression and not at all what you appear to be.’ Next, examine and test it by the rules you possess, the first and greatest of which is this—whether it belongs to the things in our control or not in our control, and if the latter, be prepared to respond, ‘It is nothing to me.’”

Part of Stoicism is cultivating the awareness that allows you to step back and analyze your own senses, question their accuracy, and proceed only with the positive and constructive ones. Sure, it’s tempting to throw discipline and order to the wind and go with what feels right—but if our many youthful regrets are any indication, what feels right right now doesn’t always stand up well over time. Hold your senses suspect. Again, trust, but always verify.

**JUDGMENTS CAUSE DISTURBANCE**

“It isn’t events themselves that disturb people, but only their judgments about them.”

**IF YOU WANT TO LEARN, BE HUMBLE**

“Throw out your conceited opinions, for it is impossible for a person to begin to learn what he thinks he already knows.”

If you want to learn, if you want to improve your life, seeking out teachers, philosophers, and great books is a good start. But this approach will only be effective if you’re humble and ready to let go of opinions you already have.

**REJECT TANTALIZING GIFTS**

Our attraction toward what is new and shiny can lead us into serious trouble.

**LESS IS MORE**

“Don’t act grudgingly, selfishly, without due diligence, or to be a contrarian. Don’t overdress your thought in fine language. Don’t be a person of too many words and too many deeds. . . . Be cheerful, not wanting outside help or the relief others might bring. A person needs to stand on their own, not be propped up.”

**BECOMING AN EXPERT IN WHAT MATTERS**

At the end of your time on this planet, what expertise is going to be more valuable—your understanding of matters of living and dying, or your knowledge of the ’87 Bears? Which will help your children more—your insight into happiness and meaning, or that you followed breaking political news every day for thirty years?

**PAY YOUR TAXES**

Income taxes are not the only taxes you pay in life. They are just the financial form. Everything we do has a toll attached to it. Waiting around is a tax on traveling. Rumors and gossip are the taxes that come from acquiring a public persona. Disagreements and occasional frustration are taxes placed on even the happiest of relationships. Theft is a tax on abundance and having things that other people want. Stress and problems are tariffs that come attached to success. And on and on and on.

There are many forms of taxes in life. You can argue with them, you can go to great—but ultimately futile—lengths to evade them, or you can simply pay them and enjoy the fruits of what you get to keep.

**OBSERVE CAUSE AND EFFECT**

Become an observer of your own thoughts and the actions those thoughts provoke. Where do they come from? What biases do they contain? Are they constructive or destructive? Do they cause you to make mistakes or engage in behavior you later regret? Look for patterns; find where cause meets effect.

Only when this is done can negative behavior patterns be broken; only then can real life improvements be made.

“Pay close attention in conversation to what is being said, and to what follows from any action. In the action, immediately look for the target, in words, listen closely to what’s being signaled.”

**NO HARM, NO FOUL**

It is so important to control the biases and lenses we bring to our interactions. When you hear or see something, which interpretation do you jump to? What is your default interpretation of someone else’s intentions?

If being upset or hurt is something you’d like to experience less often, then make sure your interpretations of others’ words make that possible. Choose the right inference from someone’s actions or from external events, and it’s a lot more likely that you’ll have the right response.

**OPINIONS ARE LIKE . . .**

We’re constantly looking at the world around us and putting our opinion on top of it. And our opinion is often shaped by dogma (religious or cultural), entitlements, expectations, and in some cases, ignorance.

No wonder we feel upset and angry so often! But what if we let these opinions go? Let’s try weeding (ekkoptein; cutting or knocking out) them out of our lives so that things simply are. Not good or bad, not colored with opinion or judgment. Just are.

**OUR SPHERE OF IMPULSES**

In our lives—whether we’re experiencing great power or powerlessness—it’s critical to leave room for what may happen and keep the common good and the actual worth of things front and center. And, above all, be willing to learn from anyone and everyone, regardless of their station in life.

**REAL GOOD IS SIMPLE**

“Here’s a way to think about what the masses regard as being ‘good’ things. If you would first start by setting your mind upon things that are unquestionably good—wisdom, self-control, justice, courage—with this preconception you’ll no longer be able to listen to the popular refrain that there are too many good things to experience in a lifetime.”

The “good” that the Stoics advocate is simpler and more straightforward: wisdom, self-control, justice, courage. No one who achieves these quiet virtues experiences buyer’s remorse.

**DON’T LET YOUR ATTENTION SLIDE**

Attention matters—and in an era in which our attention is being fought for by every new app, website, article, book, tweet, and post, its value has only gone up.

attention is a habit, and Ietting your attention slip and wander builds bad habits and enables mistakes.

**You’ll never complete all your tasks if you allow yourself to be distracted with every tiny interruption. Your attention is one of your most critical resources. Don’t squander it!**

**THE MARKS OF A RATIONAL PERSON**

To be rational today, we have to do just three things:

- First, we must look inward.
- Next, we must examine ourselves critically.
- Finally, we must make our own decisions—uninhibited by biases or popular notions.

**THE MIND IS ALL YOURS**

“You have been formed of three parts—body, breath, and mind. Of these, the first two are yours insofar as they are only in your care. The third alone is truly yours.”

You wouldn’t spend much time fixing up a house that you rent, would you? Our mind is ours—free and clear. Let’s make sure we treat it right.

**A PRODUCTIVE USE FOR CONTEMPT**

“Just as when meat or other foods are set before us we think, this is a dead fish, a dead bird or pig; and also, this fine wine is only the juice of a bunch of grapes, this purple-edged robe just sheep’s wool dyed in a bit of blood from a shellfish; or of sex, that it is only rubbing private parts together followed by a spasmic discharge—in the same way our impressions grab actual events and permeate them, so we see them as they really are.”

Well, if you take a second to consider sex in such an absurd light, you may be less likely to do something shameful or embarrassing in the pursuit of it. It’s a counterbalance to the natural bias we have toward something that feels really good.

We can apply this same way of thinking to a lot of things that people prize. Consider that envy-inducing photo you see on social media—imagine the person painstakingly staging it. What about that job promotion that means so much? Look at the lives of other so-called successful people. Still think it holds magical power? Money, which we want more of and are reluctant to part with—consider how covered in bacteria and filth it is.

**THERE’S NOTHING WRONG WITH BEING WRONG**

No one should be ashamed at changing his mind—that’s what the mind is for. “A foolish consistency is the hobgoblin of little minds,” Emerson said, “adored by little statesmen and philosophers and divines.” That’s why we go to such lengths to learn and expose ourselves to wisdom. It would be embarrassing if we didn’t end up finding out if we were wrong in the past.

Remember: you’re a free agent. When someone points out a legitimate flaw in your belief or in your actions, they’re not criticizing you. They’re presenting a better alternative. Accept it!

**THINGS HAPPEN IN TRAINING**

By seeing each day and each situation as a kind of training exercise, the stakes suddenly become a lot lower. The way you interpret your own mistakes and the mistakes of others is suddenly a lot more generous. It’s certainly a more resilient attitude than going around acting like the stakes of every encounter put the championship on the line.

When you catch an elbow or an unfair blow today, shake off the pain and remind yourself: I’m learning.

**TURN IT INSIDE OUT**

**Look at everything objectively without emotions. Don't get attached to either positive or negative Situations.**

Stoicism is about looking at things from every angle—and certain situations are easier to understand from different perspectives. In potentially negative situations, the objective, even superficial gaze might actually be superior. That view might let us see things clearly without diving too much into what they might represent or what might have caused them. In other situations, particularly those that involve something impressive or praiseworthy, another approach, like that of contemptuous expressions, is helpful. By examining situations from the inside out, we can be less daunted by them, less likely to be swayed by them.

Dig into your fear of death or obscurity, and what will you find? Turn some fancy ceremony inside out and you’ll find—what?

**WANTS MAKE YOU A SERVANT**

In the modern world, our interactions with tyranny are a bit more voluntary than they were in ancient times. We put up with our controlling boss, though we could probably get a different job if we wanted. We change how we dress or refrain from saying what we actually think? Because we want to fit in with some cool group. We put up with cruel critics or customers? Because we want their approval. In these cases, their power exists because of our wants. You change that, and you’re free.

Remember: taking the money, wanting the money—proverbially or literally—makes you a servant to the people who have it. Indifference to it, as Seneca put it, turns the highest power into no power, at least as far as your life is concerned.

**WASHING AWAY THE DUST OF LIFE**

Obviously, given that we’re in our bodies every day, it’s tempting to think that’s the most important thing in the world. But we counteract that bias by looking at nature—at things much bigger than us.

Looking at the beautiful expanse of the sky is an antidote to the nagging pettiness of earthly concerns. And it is good and sobering to lose yourself in that as often as you can.

**WHAT IS IN KEEPING WITH YOUR CHARACTER?**

“Just as what is considered rational or irrational differs for each person, in the same way what is good or evil and useful or useless differs for each person. This is why we need education, so that we might learn how to adjust our preconceived notions of the rational and irrational in harmony with nature. In sorting this out, we don’t simply rely on our estimate of the value of external things, but also apply the rule of what is in keeping with one’s character.”

Character is a powerful defense in a world that would love to be able to seduce you, buy you, tempt you, and change you. If you know what you believe and why you believe it, you’ll avoid poisonous relationships, toxic jobs, fair-weather friends, and any number of ills that afflict people who haven’t thought through their deepest concerns. That’s your education. That’s why you do this work.
