**FOR THE HOT-HEADED MAN**

- Strength is the ability to maintain a hold of oneself. It’s being the person who never gets mad, who cannot be rattled, because they are in control of their passions—rather than controlled by their passions.
- A real man doesn’t give way to anger and discontent, and such a person has strength, courage, and endurance—unlike the angry and complaining. The nearer a man comes to a calm mind, the closer he is to strength.”

**PROPER FRAME OF MIND**

We would never let another person jerk us around the way we let our impulses do. It’s time we start seeing it that way—that we’re not puppets that can be made to dance this way or that way just because we feel like it. We should be the ones in control, not our emotions, because we are independent, self-sufficient people.

**SOURCE OF YOUR ANXIETY**

Today, when you find yourself getting anxious, ask yourself: Why are my insides twisted into knots? Am I in control here or is my anxiety? And most important: Is my anxiety doing me any good?

**ON BEING INVINCIBLE**

- “Who then is invincible? The one who cannot be upset by anything outside their reasoned choice.”
- it might be helpful—whatever stresses or frustrations or overload that do come your way—to picture that image and use it as your model for dealing with them. Our reasoned choice—our prohairesis, as the Stoics called it—is a kind of invincibility that we can cultivate.

**STEADY YOUR IMPULSES**

Justice. Reason. Philosophy. If there’s a central message of Stoic thought, it’s this: impulses of all kinds are going to come, and your work is to control them, like bringing a dog to heel. Put more simply: think before you act. Ask: Who is in control here? What principles are guiding me?

**DON’T SEEK OUT STRIFE**

Yes, the man in the arena is admirable. As is the soldier and the politician and the businesswoman and all the other occupations. But, and this is a big but, only if we’re in the arena for the right reasons

**FEAR IS A SELF-FULFILLING PROPHECY**

The next time you are afraid of some supposedly disastrous outcome, remember that if you don’t control your impulses, if you lose your self-control, you may be the very source of the disaster you so fear. It has happened to smarter and more powerful and more successful people. It can happen to us too.

**DID THAT MAKE YOU FEEL BETTER?**

The next time you find yourself in the middle of a freakout, or moaning and groaning with flulike symptoms, or crying tears of regret, just ask: Is this actually making me feel better? Is this actually relieving any of the symptoms I wish were gone?

**YOU DON’T HAVE TO HAVE AN OPINION**

- “We have the power to hold no opinion about a thing and to not let it upset our state of mind—for things have no natural power to shape our judgments.”
- it is possible to hold no opinion about a negative thing. You just need to cultivate that power instead of wielding it accidentally. Especially when having an opinion is likely to make us aggravated.
- Practice the ability of having absolutely no thoughts about something—act as if you had no idea it ever occurred. Or that you’ve never heard of it before. Let it become irrelevant or nonexistent to you. It’ll be a lot less powerful this way.

**ANGER IS BAD FUEL**

There is no more stupefying thing than anger, nothing more bent on its own strength. If successful, none more arrogant, if foiled, none more insane—since it’s not driven back by weariness even in defeat, when fortune removes its adversary it turns its teeth on itself.

**PROTECT YOUR PEACE OF MIND**

- Don’t forget to ask: Is this really the life I want? Every time you get upset, a little bit of life leaves the body. Are these really the things on which you want to spend that priceless resource? Don’t be afraid to make a change—a big one.
- Keep constant guard over your perceptions, for it is no small thing you are protecting, but your respect, trustworthiness and steadiness, peace of mind, freedom from pain and fear, in a word your freedom. For what would you sell these things?

**PLEASURE CAN BECOME PUNISHMENT**

It’s important to connect the so-called temptation with its actual effects. Once you understand that indulging might actually be worse than resisting, the urge begins to lose its appeal. In this way, self-control becomes the real pleasure, and the temptation becomes the regret.

**THINK BEFORE YOU ACT**

- “For to be wise is only one thing—to fix our attention on our intelligence, which guides all things everywhere.”
- Within that head of yours is all the reason and intelligence you need. It’s making sure that it’s deferred to and utilized that’s the tough part. It’s making sure that your mind is in charge, not your emotions, not your immediate physical sensations, not your surging hormones.
- Fix your attention on your intelligence. Let it do its thing.

**ONLY BAD DREAMS**

Many of the things that upset us, the Stoics believed, are a product of the imagination, not reality. Like dreams, they are vivid and realistic at the time but preposterous once we come out of it. In a dream, we never stop to think and say: “Does this make any sense?” No, we go along with it. The same goes with our flights of anger or fear or other extreme emotions.

Getting upset is like continuing the dream while you’re awake. The thing that provoked you wasn’t real—but your reaction was. And so from the fake comes real consequences. Which is why you need to wake up right now instead of creating a nightmare.

**DON’T MAKE THINGS HARDER THAN THEY NEED TO BE**

- Don't become emotional for every Other thing . Be practical, realistic.
- Life (and our job) is difficult enough. Let’s not make it harder by getting emotional about insignificant matters or digging in for battles we don’t actually care about. Let’s not let emotion get in the way of kathêkon, the simple, appropriate actions on the path to virtue.

**THE ENEMY OF HAPPINESS**

Eagerly anticipating some future event, passionately imagining something you desire, looking forward to some happy scenario—as pleasurable as these activities might seem, they ruin your chance at happiness here and now. Locate that yearning for more, better, someday and see it for what it is: the enemy of your contentment. Choose it or your happiness. As Epictetus says, the two are not compatible.

**PREPARE FOR THE STORM**

“This is the true athlete—the person in rigorous training against false impressions. Remain firm, you who suffer, don’t be kidnapped by your impressions! The struggle is great, the task divine—to gain mastery, freedom, happiness, and tranquility.”

**THE GRAND PARADE OF DESIRE**

Ask yourself: Is that really worth it? Is it really that pleasurable? Consider that when you crave something or contemplate indulging in a “harmless” vice.

**WISH NOT, WANT NOT**

When it comes to your goals and the things you strive for, ask yourself: Am I in control of them or they in control of me?

**WHAT’S BETTER LEFT UNSAID**

It’s easy to act—to just dive in. It’s harder to stop, to pause, to think: No, I’m not sure I need to do that yet. I’m not sure I am ready.

To do this requires awareness. It requires us to stop and evaluate ourselves honestly. Can you do that?

**CIRCUMSTANCES HAVE NO CARE FOR OUR FEELINGS**

Why bother getting mad at causes and forces far bigger than us? Why do we take these things personally? After all, external events are not sentient beings—they cannot respond to our shouts and cries—and neither can the mostly indifferent gods.

circumstances are incapable of considering or caring for your feelings, your anxiety, or your excitement. They don’t care about your reaction. They are not people. So stop acting like getting worked up is having an impact on a given situation. Situations don’t care at all.

**THE REAL SOURCE OF HARM**

“Keep in mind that it isn’t the one who has it in for you and takes a swipe that harms you, but rather the harm comes from your own belief about the abuse. So when someone arouses your anger, know that it’s really your own opinion fueling it. Instead, make it your first response not to be carried away by such impressions, for with time and distance self-mastery is more easily achieved.”

Our reaction is what actually decides whether harm has occurred. If we feel that we’ve been wronged and get angry, of course that’s how it will seem. If we raise our voice because we feel we’re being confronted, naturally a confrontation will ensue.

But if we retain control of ourselves, we decide whether to label something good or bad. In fact, if that same event happened to us at different points in our lifetime, we might have very different reactions. So why not choose now to not apply these labels? Why not choose not to react?

**THE SMOKE AND DUST OF MYTH**

Eventually, all of us will pass away and slowly be forgotten. We should enjoy this brief time we have on earth—not be enslaved to emotions that make us miserable and dissatisfied.

**TO EACH HIS OWN**

“Another has done me wrong? Let him see to it. He has his own tendencies, and his own affairs. What I have now is what the common nature has willed, and what I endeavor to accomplish now is what my nature wills.”

it’s easy to fight back. It’s tempting to give them a piece of your mind. But you almost always end up with regret. You almost always wish you hadn’t sent the letter. Think of the last time you flew off the handle. What was the outcome? Was there any benefit?

**CULTIVATING INDIFFERENCE WHERE OTHERS GROW PASSION**

“Of all the things that are, some are good, others bad, and yet others indifferent. The good are virtues and all that share in them; the bad are the vices and all that indulge them; the indifferent lie in between virtue and vice and include wealth, health, life, death, pleasure, and pain.”

It’s not about avoidance or shunning, but rather not giving any possible outcome more power or preference than is appropriate. This not easy to do, certainly, but if you could manage, how much more relaxed would you be?

**WHEN YOU LOSE CONTROL**

Remember that the tools and aims of our training are unaffected by the turbulence of the moment. Stop. Regain your composure. It’s waiting for you.

**YOU CAN’T ALWAYS (BE) GET(TING) WHAT YOU WANT**

“When children stick their hand down a narrow goody jar they can’t get their full fist out and start crying. Drop a few treats and you will get it out! Curb your desire—don’t set your heart on so many things and you will get what you need.”

“Don’t set your heart on so many things,” says Epictetus. Focus. Prioritize. Train your mind to ask: Do I need this thing? What will happen if I do not get it? Can I make do without it?

The answers to these questions will help you relax, help you cut out all the needless things that make you busy—too busy to be balanced or happy.
