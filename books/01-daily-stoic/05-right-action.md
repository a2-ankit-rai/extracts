**MAKE CHARACTER YOUR LOUDEST STATEMENT**

“For philosophy doesn’t consist in outward display, but in taking heed to what is needed and being mindful of it.”

A Stoic has no uniform and resembles no stereotype. They are not identifiable by look or by sight or by sound.

The only way to recognize them? By their character.

**BE THE PERSON YOU WANT TO BE**

Our perceptions and principles guide us in the selection of what we want—but ultimately our actions determine whether we get there or not.

So yes, spend some time—real, uninterrupted time—thinking about what’s important to you, what your priorities are. Then, work toward that and forsake all the others. It’s not enough to wish and hope. One must act—and act right.

**SHOW, NOT TELL, WHAT YOU KNOW**

Today, or anytime, when you catch yourself wanting to condescendingly drop some knowledge that you have, grab it and ask: Would I be better saying words or letting my actions and choices illustrate that knowledge for me?

**YOU ARE THE PROJECT**

“The raw material for the work of a good and excellent person is their own guiding reason, the body is that of the doctor and the physical trainer, and the farm the farmer’s.”

According to the Stoics, your mind is the asset that must be worked on most—and understood best.

**RIGHTEOUSNESS IS BEAUTIFUL**

“Then what makes a beautiful human being? Isn’t it the presence of human excellence? Young friend, if you wish to be beautiful, then work diligently at human excellence. And what is that? Observe those whom you praise without prejudice. The just or the unjust? The just. The even-tempered or the undisciplined? The even-tempered. The self-controlled or the uncontrolled? The self-controlled. In making yourself that kind of person, you will become beautiful—but to the extent you ignore these qualities, you’ll be ugly, even if you use every trick in the book to appear beautiful.”

**HOW TO HAVE A GOOD DAY**

Here is how to guarantee you have a good day: do good things.

Any other source of joy is outside your control or is nonrenewable. But this one is all you, all the time, and unending. It is the ultimate form of self-reliance.

**GOOD AND EVIL? LOOK AT YOUR CHOICES**

Remember: the right thing to do always comes from our reasoned choice. Not whether something is rewarded. Not whether something will succeed, but whether it is the right choice.

Ignore everything else. Focus only on your choices.

**CARPE DIEM****

is gone and lost forever. Will you fully inhabit all of today? Will you call out, “I’ve got this,” and do your very best to be your very best?

What will you manage to make of today before it slips from your fingers and becomes the past? When someone asks you what you did yesterday, do you really want the answer to be “nothing”?

**DON’T BE INSPIRED, BE INSPIRATIONAL**

“Let us also produce some bold act of our own—and join the ranks of the most emulated.”

**GUILT IS WORSE THAN JAIL**

“The greatest portion of peace of mind is doing nothing wrong. Those who lack self-control live disoriented and disturbed lives.”

**KINDNESS IS ALWAYS THE RIGHT RESPONSE**

Most rudeness, meanness, and cruelty are a mask for deep-seated weakness. Kindness in these situations is only possible for people of great strength. You have that strength. Use it.

**FUELING THE HABIT BONFIRE**

Think about your activities of the last week as well as what you have planned for today and the week that follows. The person you’d like to be, or the person you see yourself as—how closely do your actions actually correspond to him or her? Which fire are you fueling? Which person are you becoming?

**OUR WELL-BEING LIES IN OUR ACTIONS**

we should take pleasure from our actions—in taking the right actions—rather than the results that come from them.

Our ambition should not be to win, then, but to play with our full effort. Our intention is not to be thanked or recognized, but to help and to do what we think is right. Our focus is not on what happens to us but on how we respond. In this, we will always find contentment and resilience.

**COUNT YOUR BLESSINGS**

Fight your urge to gather and hoard. That’s not the right way to live and act. Appreciate and take advantage of what you already do have, and let that attitude guide your actions.

**THE CHAIN METHOD**

Start with one day doing whatever it is, be it managing your temper or wandering eyes or procrastination. Then do the same the following day and the day after that. Build a chain and then work not to break it. Don’t ruin your streak.

**THE STOIC IS A WORK IN PROGRESS**

It’s important for us to remember in our own journey to self-improvement: one never arrives. The sage—the perfect Stoic who behaves perfectly in every situation—is an ideal, not an end.

**HOW YOU DO ANYTHING IS HOW YOU DO EVERYTHING**

It’s fun to think about the future. It’s easy to ruminate on the past. It’s harder to put that energy into what’s in front of us right at this moment—especially if it’s something we don’t want to do. We think: This is just a job; it isn’t who I am. It doesn’t matter. But it does matter. Who knows—it might be the last thing you ever do. Here lies Dave, buried alive under a mountain of unfinished business.

There is an old saying: “How you do anything is how you do everything.” It’s true. How you handle today is how you’ll handle every day. How you handle this minute is how you’ll handle every minute.

**LEARN, PRACTICE, TRAIN**

“That’s why the philosophers warn us not to be satisfied with mere learning, but to add practice and then training. For as time passes we forget what we learned and end up doing the opposite, and hold opinions the opposite of what we should.”

Very few people can simply watch an instructional video or hear something explained and then know, backward and forward, how to do it. Most of us actually have to do something several times in order to truly learn. One of the hallmarks of the martial arts, military training, and athletic training of almost any kind is the hours upon hours upon hours of monotonous practice. An athlete at the highest level will train for years to perform movements that can last mere seconds—or less. The two-minute drill, how to escape from a chokehold, the perfect jumper. Simply knowing isn’t enough. It must be absorbed into the muscles and the body. It must become part of us. Or we risk losing it the second that we experience stress or difficulty.

**QUALITY OVER QUANTITY**

What if, when it came to your reading and learning, you prioritized quality over quantity? What if you read the few great books deeply instead of briefly skimming all the new books? Your shelves might be emptier, but your brain and your life would be fuller.

**WHAT KIND OF BOXER ARE YOU?**

Seneca writes that unbruised prosperity is weak and easy to defeat in the ring, but “a man who has been at constant feud with misfortunes acquires a skin calloused by suffering.” This man, he says, fights all the way to the ground and never gives up.

What kind of boxer are you if you leave because you get hit? That’s the nature of the sport! Is that going to stop you from continuing?

**TODAY IS THE DAY**

We almost always know what the right thing is. We know we should not get upset, that we shouldn’t take this personally, that we should walk to the health food store instead of swinging by the drive-through, that we need to sit down and focus for an hour. The tougher part is deciding to do it in a given moment.

Today, not tomorrow, is the day that we can start to be good.

**SHOW ME HOW TO LIVE**

“Show me that the good life doesn’t consist in its length, but in its use, and that it is possible—no, entirely too common—for a person who has had a long life to have lived too little.”

The best way to get there is by focusing on what is here right now, on the task you have at hand—big or small. As he says, by pouring ourselves fully and intentionally into the present, it “gentle[s] the passing of time’s precipitous flight.”

**MAKING YOUR OWN GOOD FORTUNE**

“You say, good fortune used to meet you at every corner. But the fortunate person is the one who gives themselves a good fortune. And good fortunes are a well-tuned soul, good impulses and good actions.”

“Diligence is the mother of good luck.”

“I am a great believer in luck. The harder I work, the more of it I seem to have.”

“Luck is where hard work meets opportunity.”

Today, you can hope that good fortune and good luck magically come your way. Or you can prepare yourself to get lucky by focusing on doing the right thing at the right time—and, ironically, render luck mostly unnecessary in the process.

**WHERE TO FIND JOY**

“Joy for human beings lies in proper human work. And proper human work consists in: acts of kindness to other human beings, disdain for the stirrings of the senses, identifying trustworthy impressions, and contemplating the natural order and all that happens in keeping with it.”

**STOP CARING WHAT PEOPLE THINK**

Although we control our own opinions, we don’t control what other people think—about us least of all. For this reason, putting ourselves at the mercy of those opinions and trying to gain the approval of others are a dangerous endeavor.

Don’t spend much time thinking about what other people think. Think about what you think. Think instead about the results, about the impact, about whether it is the right thing to do.

**SWEAT THE SMALL STUFF**

The little things add up. Someone is a good person not because they say they are, but because they take good actions. One does not magically get one’s act together—it is a matter of many individual choices. It’s a matter of getting up at the right time, making your bed, resisting shortcuts, investing in yourself, doing your work. And make no mistake: while the individual action is small, its cumulative impact is not.

Think about all the small choices that will roll themselves out in front of you today. Do you know which are the right way and which are the easy way? Choose the right way, and watch as all these little things add up toward transformation.

**THE FIRST TWO THINGS BEFORE ACTING**

First, don’t get upset—because that will color your decision negatively and make it harder than it needs to be.

Second, remember the purpose and principles you value most. Running potential actions through this filter will eliminate the bad choices and highlight the right ones.

Don’t get upset.

Do the right thing.

That’s it.

**WORK IS THERAPY**

The mind and the body are there to be used—they begin to turn on themselves when not put to some productive end.

The solution is simple and, thankfully, always right at hand. Get out there and work.

**WORKING HARD OR HARDLY WORKING?**

Evaluate what you are doing, why you are doing it, and where accomplishing it will take you. If you don’t have a good answer, then stop.
