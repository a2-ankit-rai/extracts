**ALWAYS HAVE A MENTAL REVERSE CLAUSE**

Obstacles are a part of life—things happen, stuff gets in our way, situations go awry. But nothing can stop the Stoic mind when it’s operating properly, because in every course of action, it has retained “a reverse clause.”

What’s that? It’s a backup option. Our progress can be impeded or disrupted, but the mind can always be changed—it retains the power to redirect the path.

Part of this is remembering the usual course of things—Murphy’s Law states that “if anything can go wrong, it will.” So we keep this reverse clause handy because we know we’re probably going to have to use it. No one can thwart that.

**PLATO’S VIEW**

Whenever you want to talk about people, it’s best to take a bird’s-eye view and see everything all at once—of gatherings, armies, farms, weddings and divorces, births and deaths, noisy courtrooms or silent spaces, every foreign people, holidays, memorials, markets—all blended together and arranged in a pairing of opposites.

**IT IS WELL TO BE FLEXIBLE**

the flexibility and determination of Stoicism. If we can’t do this, then perhaps we can try that. And if we can’t do that, then perhaps we can try some other thing. And if that thing is impossible, there is always another. Even if that final thing is just being a good human being—we always have some opportunity to practice our philosophy, to make some contribution.

**THIS IS WHAT WE’RE HERE FOR**

No one said life was easy. No one said it would be fair.

Don’t forget, though, that you come from a long, unbroken line of ancestors who survived unimaginable adversity, difficulty, and struggle. It’s their genes and their blood that run through your body right now. Without them, you wouldn’t be here.

You’re an heir to an impressive tradition—and as their viable offspring, you’re capable of what they are capable of. You’re meant for this. Bred for it.

Just something to keep in mind if things get tough.

**BLOW YOUR OWN NOSE**

“We cry to God Almighty, how can we escape this agony? Fool, don’t you have hands? Or could it be God forgot to give you a pair? Sit and pray your nose doesn’t run! Or, rather just wipe your nose and stop seeking a scapegoat.”

We have a choice: Do we focus on the ways we have been wronged, or do we use what we’ve been given and get to work? Will we wait for someone to save us, or will we listen to Marcus Aurelius’s empowering call to “get active in your own rescue—if you care for yourself at all—and do it while you can.” That’s better than just blowing your own nose (which is a step forward in itself).

**WHEN TO STICK AND WHEN TO QUIT**

Just because you’ve begun down one path doesn’t mean you’re committed to it forever, especially if that path turns out to be flawed or impeded. At that same time, this is not an excuse to be flighty or incessantly noncommittal. It takes courage to decide to do things differently and to make a change, as well as discipline and awareness to know that the notion of “Oh, but this looks even better” is a temptation that cannot be endlessly indulged either.

**FINDING THE RIGHT MENTORS**

“We like to say that we don’t get to choose our parents, that they were given by chance—yet we can truly choose whose children we’d like to be.”

Maybe your parents were poor role models, or you lacked a great mentor. Yet if we choose to, we can easily access the wisdom of those who came before us—those whom we aspire to be like.

**BRICK BY BORING BRICK**

“You must build up your life action by action, and be content if each one achieves its goal as far as possible—and no one can keep you from this. But there will be some external obstacle! Perhaps, but no obstacle to acting with justice, self-control, and wisdom. But what if some other area of my action is thwarted? Well, gladly accept the obstacle for what it is and shift your attention to what is given, and another action will immediately take its place, one that better fits the life you are building.”

Focus on doing the absolutely smallest things well—practicing with full effort, finishing a specific play, converting on a single possession.

Follow The Process in your life—assembling the right actions in the right order, one right after another—you too will do well. Not only that, you will be better equipped to make quick work of the obstacles along that path. You’ll be too busy putting one foot in front of the next to even notice the obstacles were there.

**SOLVE PROBLEMS EARLY**

“There is no vice which lacks a defense, none that at the outset isn’t modest and easily intervened—but after this the trouble spreads widely. If you allow it to get started you won’t be able to control when it stops. Every emotion is at first weak. Later it rouses itself and gathers strength as it moves along—it’s easier to slow it down than to supplant it.”

“Rivers are easiest to cross at their source.”

The raging waters and deadly currents of bad habits, ill discipline, chaos, and dysfunction—somewhere they began as no more than just a slight trickle. Somewhere they are a placid lake or pond, even a bubbling underground spring.

Which would you rather do—nearly drown in a dangerous crossing in a few weeks or cross now while it’s still easy? It’s up to you.

**YOU CAN DO IT**

“If you find something very difficult to achieve yourself, don’t imagine it impossible—for anything possible and proper for another person can be achieved as easily by you.”

**JUST DON’T MAKE THINGS WORSE**

The first rule of holes, goes the adage, is that “if you find yourself in a hole, stop digging.” This might be the most violated piece of commonsense wisdom in the world. Because what most of us do when something happens, goes wrong, or is inflicted on us is make it worse—first, by getting angry or feeling aggrieved, and next, by flailing around before we have much in the way of a plan.

Today, give yourself the most simple and doable of tasks: just don’t make stuff worse. Whatever happens, don’t add angry or negative emotions to the equation. Don’t react for the sake of reacting. Leave it as it is. Stop digging. Then plan your way out.

**A TRAINED MIND IS BETTER THAN ANY SCRIPT**

Stoics do not seek to have the answer for every question or a plan for every contingency. Yet they’re also not worried. Why? Because they have confidence that they’ll be able to adapt and change with the circumstances. Instead of looking for instruction, they cultivate skills like creativity, independence, self-confidence, ingenuity, and the ability to problem solve. In this way, they are resilient instead of rigid. We can practice the same.

Today, we will focus on the strategic rather than the tactical. We’ll remind ourselves that it’s better to be taught than simply given, and better to be flexible than stick to a script.

**LIFE IS A BATTLEFIELD**

our life is a battle both literally and figuratively. As a species, we fight to survive on a planet indifferent to our survival. As individuals, we fight to survive among a species whose population numbers in the billions. Even inside our own bodies, diverse bacteria battle it out. Vivere est militare. (To live is to fight.)

Today, you’ll be fighting for your goal, fighting against impulses, fighting to be the person you want to be. So what are the attributes necessary to win these many wars?

- Discipline
- Fortitude
- Courage
- Clearheadedness
- Selflessness
- Sacrifice

And which attributes lose wars?

- Cowardice
- Rashness
- Disorganization
- Overconfidence
- Weakness
- Selfishness

**TRY THE OTHER HANDLE**

“Every event has two handles—one by which it can be carried, and one by which it can’t. If your brother does you wrong, don’t grab it by his wronging, because this is the handle incapable of lifting it. Instead, use the other—that he is your brother, that you were raised together, and then you will have hold of the handle that carries.”

**LISTENING ACCOMPLISHES MORE THAN SPEAKING**

Why do the wise have so few problems compared with the rest of us? There are a few simple reasons.

First, the wise seem to manage expectations as much as possible. They rarely expect what isn’t possible in the first place.

Second, the wise always consider both the best and worst case scenarios. They don’t just think about what they wish to happen, but also what very realistically can happen if things were to suddenly turn.

Third, the wise act with a reverse clause—meaning that they not only consider what might go wrong, but they are prepared for that to be exactly what they want to happen—it is an opportunity for excellence and virtue.

And if you follow it today, you too will find that nothing surprises you or happens contrary to your expectations.

**NO SHAME IN NEEDING HELP**

“Don’t be ashamed of needing help. You have a duty to fulfill just like a soldier on the wall of battle. So what if you are injured and can’t climb up without another soldier’s help?”

**PREPARED AND ACTIVE**

Whatever happens today, let it find us prepared and active: ready for problems, ready for difficulties, ready for people to behave in disappointing or confusing ways, ready to accept and make it work for us. Let’s not wish we could turn back time or remake the universe according to our preference. Not when it would be far better and far easier to remake ourselves.

**STAY FOCUSED ON THE PRESENT**

“Don’t let your reflection on the whole sweep of life crush you. Don’t fill your mind with all the bad things that might still happen. Stay focused on the present situation and ask yourself why it’s so unbearable and can’t be survived.”

“The trick to forgetting the big picture is to look at everything close up.” Sometimes grasping the big picture is important, and the Stoics have helped us with that before. A lot of times, though, it’s counterproductive and overwhelming to be thinking of everything that lies ahead. So by focusing exclusively on the present, we’re able to avoid or remove those intimidating or negative thoughts from our frame of view.

**CALM IS CONTAGIOUS**

“If then it’s not that the things you pursue or avoid are coming at you, but rather that you in a sense are seeking them out, at least try to keep your judgment of them steady, and they too will remain calm and you won’t be seen chasing after or fleeing from them.”

That’s who you want to be, whatever your line of work: the casual, relaxed person in every situation who tells everyone else to take a breath and not to worry. Because you’ve got this. Don’t be the agitator, the paranoid, the worrier, or the irrational. Be the calm, not the liability.

It will catch on.

**TAKE A WALK**

Throughout the ages, philosophers, writers, poets, and thinkers have found that walking offers an additional benefit—time and space for better work.

Today, make sure you take a walk. And in the future, when you get stressed or overwhelmed, take a walk. When you have a tough problem to solve or a decision to make, take a walk. When you want to be creative, take a walk. When you need to get some air, take a walk. When you have a phone call to make, take a walk. When you need some exercise, take a long walk. When you have a meeting or a friend over, take a walk together.

Nourish yourself and your mind and solve your problems along the way.

**THE DEFINITION OF INSANITY**

definition of insanity is trying the same thing over and over again but expecting a different result. Yet that’s exactly what most people do. They tell themselves: Today, I won’t get angry. Today, I won’t gorge myself. But they don’t actually do anything differently. They try the same routine and hope it will work this time. Hope is not a strategy!

Failure is a part of life we have little choice over. Learning from failure, on the other hand, is optional. We have to choose to learn. We must consciously opt to do things differently—to tweak and change until we actually get the result we’re after.

Sticking with the same unsuccessful pattern is easy. It doesn’t take any thought or any additional effort, which is probably why most people do it.

**THE LONG WAY AROUND**

“You could enjoy this very moment all the things you are praying to reach by taking the long way around—if you’d stop depriving yourself of them.”

- Freedom? That’s easy. It’s in your choices.
- Happiness? That’s easy. It’s in your choices.
- Respect of your peers? That too is in the choices you make.

And all of that is right in front of you. No need to take the long way to get there.

**THE TRULY EDUCATED AREN’T QUARRELSOME**

“The beautiful and good person neither fights with anyone nor, as much as they are able, permits others to fight . . . this is the meaning of getting an education—learning what is your own affair and what is not. If a person carries themselves so, where is there any room for fighting?”

The next time you face a political dispute or a personal disagreement, ask yourself: Is there any reason to fight about this? Is arguing going to help solve anything? Would an educated or wise person really be as quarrelsome as you might initially be inclined to be? Or would they take a breath, relax, and resist the temptation for conflict? Just think of what you could accomplish—and how much better you would feel—if you could conquer the need to fight and win every tiny little thing.

**THE WISE DON’T HAVE “PROBLEMS”**

Always Say Less Than Necessary.

We talk because we think it’s helping, whereas in reality it’s making things hard for us.

In other situations, the world is trying to give us feedback or input, but we try to talk ourselves out of the problem—only to make it worse.

So today, will you be part of the problem or part of the solution? Will you hear the wisdom of the world or drown it out with more noise?

**TRY THE OPPOSITE**

“What assistance can we find in the fight against habit? Try the opposite!”

In your fight against a habit, try to do the opposite . This might work when you have tried every thing else.

As an example, suppose you are not able to exercise due to lazyness. In try the opposite method, try not to do any exercise at all in any manner. Your body will then forced to exercise & that's how you will fight this habit.

**ADVERSITY REVEALS**

How you handle even minor adversity might seem like nothing, but, in fact, it reveals everything.

**NO SELF-FLAGELLATION NEEDED**

Self-criticism should be constructive.

Laying into yourself, unduly depriving yourself, punishing yourself—that’s self-flagellation, not self-improvement.

No need to be too hard on yourself. Hold yourself to a higher standard but not an impossible one. And forgive yourself if and when you slip up.

Don't be too hard on yourself when things don't work out. Don't deprive yourself of simple things. Take your time. Have a high self-esteem for yourself. But also, be realistic.

**NO EXCUSES**

“It is possible to curb your arrogance, to overcome pleasure and pain, to rise above your ambition, and to not be angry with stupid and ungrateful people—yes, even to care for them.”

**THE OBSTACLE IS THE WAY**

“While it’s true that someone can impede our actions, they can’t impede our intentions and our attitudes, which have the power of being conditional and adaptable. For the mind adapts and converts any obstacle to its action into a means of achieving it. That which is an impediment to action is turned to advance action. The obstacle on the path becomes the way.”

Today, things will happen that will be contrary to your plans. If not today, then certainly tomorrow. As a result of these obstacles, you will not be able to do what you planned. This is not as bad as it seems, because your mind is infinitely elastic and adaptable.

- If something prevents you from getting to your destination on time, then this is a chance to practice patience.
- If an employee makes an expensive mistake, this is a chance to teach a valuable lesson.
- If a computer glitch erases your work, it’s a chance to start over with a clean slate.
- If someone hurts you, it’s a chance to practice forgiveness.
- If something is hard, it is a chance to get stronger.

Try this line of thinking and see whether there is a situation in which one could not find some virtue to practice or derive some benefit. There isn’t one. Every impediment can advance action in some form or another.
